/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.shapeproject;

/**
 *
 * @author MSI GAMING
 */
public class Rectangle {
    private double w;
    private double l;
    
public Rectangle(double w, double l){
    this.w = w;
    this.l = l;
}
public double calArea(){
    return w*l ;
}
public double getOutput(){
        return w;
    }
public double getOutput1(){
        return l;
    }
    public void setInput(double w, double l){
        if(w<=0 || l<=0){
            System.out.println("Error: width and length must more than zero!!!");
            return;
        }
        this.w = w;
        this.l = l;
    }
}
