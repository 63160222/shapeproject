/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.shapeproject;

/**
 *
 * @author MSI GAMING
 */
public class Triangle {
    private double b;
    private double h;
    public Triangle(double b,double h){
        this.b = b;
        this.h = h;
    }
    public double calArea(){
        return   0.5 * b * h;
        
    }
    public double getOutput(){
        return b;
    }
    public double getOutput1(){
        return h;
    }
    public void setInput(double b, double h){
        if(b<=0 || h<=0){
            System.out.println("Error: base and height must more than zero!!!");
            return;
        }
        this.b = b;
        this.h = h;
    }
    
}
